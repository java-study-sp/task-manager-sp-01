<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2019
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task Create</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="/main">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link active" href="/projects">Project List <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link active" href="/tasks">Task List <span class="sr-only">(current)</span></a>
        </div>
    </div>
</nav>
<div class="container">
    <div class="mt-1 ml-3">
        <h1>Task Create</h1>
    </div>
    <div class="mt-1 ml-3">
        <form action="/tasks/create" method="post">
            <div class="form-group col-6">
                <label for="formGroupExampleInput">Enter task Name</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Task name"
                       name="taskName">
            </div>
            <c:choose>
                <c:when test="${!idProjectList.isEmpty()}">
                    <div class="form-group col-6">
                        <label for="exampleFormControlSelect1">Project ID list</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="projectId">
                            <c:forEach items="${idProjectList}" var="projectId">
                                <option>${projectId}</option>
                            </c:forEach>
                        </select>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-group col-6">
                        <label for="formGroupExampleInput1">Enter project ID</label>
                        <input type="text" class="form-control" id="formGroupExampleInput1" placeholder="Project ID"
                               name="projectId">
                    </div>
                </c:otherwise>
            </c:choose>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
</div>
</body>
</html>
