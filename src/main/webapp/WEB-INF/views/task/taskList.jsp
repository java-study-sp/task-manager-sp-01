<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2019
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<html>
<head>
    <title>Task List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="/main">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link active" href="/projects">Project List <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link active" href="/tasks">Task List <span class="sr-only">(current)</span></a>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div>
        <h1>Task List</h1>
    </div>
    <div>
        <c:choose>
            <c:when test="${!tasklist.isEmpty()}">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Project ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Data Create</th>
                        <th scope="col">Data Begin</th>
                        <th scope="col">Data End</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${tasklist}" var="task">
                        <tr>
                            <td scope="row">${task.id}</td>
                            <td scope="row">${task.idProject}</td>
                            <td scope="row">${task.name}</td>
                            <td scope="row">${task.description}</td>
                            <td scope="row">${task.createdData}</td>
                            <td scope="row">${task.startDate}</td>
                            <td scope="row">${task.endDate}</td>
                            <td scope="row">${task.status}</td>
                            <td scope="row"><a href="/tasks/view/${task.id}/">VIEW</a></td>
                            <td scope="row"><a href="/tasks/edit/${task.id}/">EDIT</a></td>
                            <td scope="row"><a href="/tasks/remove/${task.id}/">REMOVE</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <h3>Task list is empty</h3>
            </c:otherwise>
        </c:choose>
        <form action="/tasks/add" method="get">
            <button type="submit" class="btn btn-primary">Create Task</button>
        </form>
    </div>
</body>
</html>
