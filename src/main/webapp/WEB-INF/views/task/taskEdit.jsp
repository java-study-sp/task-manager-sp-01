<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2019
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task Edit</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="/main">Home <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link active" href="/projects">Project List <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link active" href="/tasks">Task List <span class="sr-only">(current)</span></a>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="mt-1 ml-3">
        <h1>Task edit</h1>
    </div>
    <div class="mt-1 ml-3">
        <form action="/tasks/save" method="post">
            <div class="form-group col-6">
                <label for="formGroupExampleInput">Task ID</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Task ID"
                       name="id" value="${task.id}" readonly>
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput0">Project ID</label>
                <input type="text" class="form-control" id="formGroupExampleInput0" placeholder="Project ID"
                       name="projectId" value="${task.idProject}" readonly>
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput1">Task name</label>
                <input type="text" class="form-control" id="formGroupExampleInput1" placeholder="Task name"
                       name="name" value="${task.name}">
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput2">Task description</label>
                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Task description"
                       name="description" value="${task.description}">
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput3">Task Date Create</label>
                <input type="text" class="form-control" id="formGroupExampleInput3" placeholder="Task Date Create"
                       name="dataCreate" value="${dataCreate}" readonly>
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput4">Task Date Begin</label>
                <input type="date" class="form-control" id="formGroupExampleInput4" placeholder="Task Date Begin"
                       name="dataBegin" value="${dataBegin}">
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput5">Task Date End</label>
                <input type="date" class="form-control" id="formGroupExampleInput5" placeholder="Task Date End"
                       name="dataEnd" value="${dataEnd}">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
        <h5><a href="/tasks">Назад</a></h5>
    </div>
</div>
</body>
</html>
