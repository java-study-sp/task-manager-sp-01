package com.morozov.tm.repository;

import com.morozov.tm.api.repository.IProjectRepository;
import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Repository
public class ProjectRepository implements IProjectRepository {
    @NotNull
    private final Map<String, Project> entityMap = new HashMap<>();

    @Override
    @NotNull
    public List<Project> findAll() {
        return new ArrayList<>(entityMap.values());
    }

    @Override
    @Nullable
    public Project findOne(@NotNull String id) {
        return entityMap.get(id);
    }


    @Override
    public void merge(@NotNull String id, @NotNull Project updateEntity) {
        entityMap.put(id, updateEntity);
    }


    @Override
    public void persist(@NotNull String id, @NotNull Project writeEntity) {
        entityMap.put(id, writeEntity);
    }


    @Override
    public void remove(@NotNull String id) {
        entityMap.remove(id);
    }


    @Override
    public void removeAll() {
        entityMap.clear();
    }

    @Override
    @NotNull
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projectListByUserID = new ArrayList<>();
        for (@NotNull final Project project : findAll()) {
            if (project.getUserId().equals(userId)) projectListByUserID.add(project);
        }
        return projectListByUserID;
    }

    @Override
    @Nullable
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        Project resultProject = null;
        for (@NotNull final Project project : findAllByUserId(userId)) {
            if (project.getId().equals(id)) resultProject = project;
        }
        return resultProject;
    }

    @Override
    @NotNull
    public List<Project> searchByString(@NotNull final String userId, @NotNull final String string) {
        @NotNull final List<Project> resultProjectList = new ArrayList<>();
        for (@NotNull final Project project : findAllByUserId(userId)) {
            if (project.getName().contains(string) || project.getDescription().contains(string))
                resultProjectList.add(project);
        }
        return resultProjectList;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projectListForDelete = findAllByUserId(userId);
        for (@NotNull final Project project : projectListForDelete) {
            entityMap.remove(project.getId());
        }
    }
}
