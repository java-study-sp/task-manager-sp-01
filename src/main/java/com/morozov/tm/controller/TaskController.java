package com.morozov.tm.controller;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TaskController {
    @Autowired
    private ITaskService taskService;
    @Autowired
    private IProjectService projectService;

    @RequestMapping(value = "")
    public String taskList(Model model) {
        @NotNull final List<Task> taskList = taskService.findAllTask();
        model.addAttribute("tasklist", taskList);
        return "task/taskList";
    }

    @RequestMapping(value = "/add")
    public String addTask(Model model) {
        @NotNull final List<String> idProjectList = new ArrayList<>();
        for (@NotNull final Project project : projectService.findAllProject()) {
            idProjectList.add(project.getId());
        }
        model.addAttribute("idProjectList", idProjectList);
        return "task/taskCreate";
    }

    @PostMapping(value = "/create")
    public String createTask(
            @RequestParam("taskName") String taskName,
            @RequestParam("projectId") String projectId
    ) {
        taskService.addTask(taskName, projectId);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/view/{taskId}")
    public String taskView(@PathVariable("taskId") String id, Model model) {
        @Nullable final Task task = taskService.findById(id);
        model.addAttribute("task", task);
        return "task/taskView";
    }

    @RequestMapping(value = "/edit/{taskId}")
    public String taskEdit(@PathVariable("taskId") String id, Model model) {
        @Nullable final Task task = taskService.findById(id);
        @Nullable final String dataBegin = DateFormatUtil.formattedDataToString(task.getStartDate());
        @Nullable final String dataEnd = DateFormatUtil.formattedDataToString(task.getEndDate());
        @Nullable final String dataCreate = DateFormatUtil.formattedDataToString(task.getCreatedData());
        model.addAttribute("task", task);
        model.addAttribute("dataCreate", dataCreate);
        model.addAttribute("dataBegin", dataBegin);
        model.addAttribute("dataEnd", dataEnd);
        return "task/taskEdit";
    }

    @PostMapping(value = "/save")
    public String taskUpdate(
            @RequestParam("id") String taskId,
            @RequestParam("name") String taskName,
            @RequestParam("description") String taskDescription,
            @RequestParam("dataBegin") String dataBegin,
            @RequestParam("dataEnd") String dataEnd,
            @RequestParam("projectId") String projectId
    ) throws ParseException {
        taskService.updateTask(taskId, taskName, taskDescription, dataBegin, dataEnd, projectId);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/remove/{taskId}")
    public String removeProject(
            @PathVariable("taskId") String id
    ) {
        taskService.removeTaskById(id);
        return "redirect:/tasks";
    }
}
