package com.morozov.tm.controller;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.util.List;

@Controller
@RequestMapping(value = "/projects")
public class ProjectController {
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ITaskService taskService;

    @RequestMapping(value = "")
    public ModelAndView projectList() {
        @Nullable final List<Project> projectList = projectService.findAllProject();
        final ModelAndView modelAndView = new ModelAndView("project/projectList");
        modelAndView.addObject("projectlist", projectList);
        return modelAndView;
    }

    @RequestMapping(value = "/view/{projectId}")
    public String projectView(@PathVariable("projectId") String id, Model model) {
        @Nullable final Project project = projectService.findById(id);
        @NotNull final List<Task> taskList = taskService.getAllTaskByProjectId(id);
        model.addAttribute("project", project);
        model.addAttribute("taskList", taskList);
        return "project/projectView";
    }

    @RequestMapping(value = "/edit/{projectId}")
    public String projectEdit(@PathVariable("projectId") String id, Model model) {
        @Nullable final Project project = projectService.findById(id);
        @Nullable final String dataBegin = DateFormatUtil.formattedDataToString(project.getStartDate());
        @Nullable final String dataEnd = DateFormatUtil.formattedDataToString(project.getEndDate());
        @Nullable final String dataCreate = DateFormatUtil.formattedDataToString(project.getCreatedData());
        model.addAttribute("project", project);
        model.addAttribute("dataCreate", dataCreate);
        model.addAttribute("dataBegin", dataBegin);
        model.addAttribute("dataEnd", dataEnd);
        return "project/projectEdit";
    }

    @PostMapping(value = "/save")
    public String projectUpdate(
            @RequestParam("id") String projectId,
            @RequestParam("name") String projectName,
            @RequestParam("description") String projectDescription,
            @RequestParam("dataBegin") String dataBegin,
            @RequestParam("dataEnd") String dataEnd
    ) throws ParseException {
        projectService.updateProject(projectId, projectName, projectDescription, dataBegin, dataEnd);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/add")
    public String addProject() {
        return "project/projectCreate";
    }

    @PostMapping(value = "/create")
    public String createProject(
            @RequestParam("projectName") String projectName
    ) {
        projectService.addProject(projectName);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/remove/{projectId}")
    public String removeProject(
            @PathVariable("projectId") String id
    ) {
        projectService.removeProjectById(id);
        return "redirect:/projects";
    }

}
