package com.morozov.tm.entity;

import org.jetbrains.annotations.NotNull;


public class Task extends AbstractWorkEntity {

    @NotNull
    private String idProject = "";

    @NotNull
    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }
}
