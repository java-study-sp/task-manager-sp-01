package com.morozov.tm.entity;


import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractEntity implements Serializable {
    @NotNull
    private String id = UUID.randomUUID().toString();

    public AbstractEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
