package com.morozov.tm.service;

import com.morozov.tm.api.repository.IProjectRepository;
import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.entity.Project;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService implements IProjectService {
    @Autowired
    private IProjectRepository projectRepository;


    @Override
    public @Nullable Project findById(String id) {
        @Nullable final Project project = projectRepository.findOne(id);
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAllProject() {
        @NotNull final List<Project> projectList = projectRepository.findAll();
        return projectList;
    }


    @Override
    @NotNull
    public List<Project> getAllProjectByUserId(@NotNull String userId) {
        @NotNull final List<Project> projectListByUserId = projectRepository.findAllByUserId(userId);
        return projectListByUserId;
    }

    @Override
    @NotNull

    public Project addProject(@NotNull final String projectName) {
        @NotNull final Project project = new Project();
        project.setName(projectName);
        projectRepository.persist(project.getId(), project);
        return project;
    }


    @Override
    public void removeProjectById(@NotNull String id) {
        if (projectRepository.findOne(id) != null) {
            projectRepository.remove(id);
        }
    }


    @Override
    public void updateProject(
            @NotNull final String id, @NotNull final String projectName,
            @NotNull final String projectDescription, @NotNull final String dataStart, @NotNull final String dataEnd)
            throws ParseException {

        final Project project = projectRepository.findOne(id);
        if (project == null) return;
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        final Date updatedStartDate = DateFormatUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            project.setStatus(StatusEnum.PROGRESS);
        }
        project.setStartDate(updatedStartDate);
        final Date updatedEndDate = DateFormatUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            project.setStatus(StatusEnum.READY);
        }
        project.setEndDate(updatedEndDate);

        projectRepository.merge(id, project);
    }

    @Override
    @NotNull
    public final List<Project> searchByString(@NotNull final String userId, @NotNull final String string) {
        if (string.isEmpty()) return projectRepository.findAllByUserId(userId);
        @NotNull final List<Project> projectListByUserId = projectRepository.searchByString(userId, string);
        return projectListByUserId;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public final void clearProjectList() {
        projectRepository.removeAll();
    }

}
