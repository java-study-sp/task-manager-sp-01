package com.morozov.tm.service;

import com.morozov.tm.api.repository.ITaskRepository;
import com.morozov.tm.entity.Task;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
@Service
public class TaskService implements com.morozov.tm.api.service.ITaskService {
    @Autowired
    private ITaskRepository taskRepository;


    @Override
    public @Nullable Task findById(String id) {
        return taskRepository.findOne(id);
    }

    @Override
    public @NotNull List<Task> findAllTask() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        return taskList;
    }


    @Override
    @NotNull
    public List<Task> getAllTask() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        return taskList;
    }


    @Override
    @NotNull
    public Task addTask(@NotNull final String taskName, @NotNull final String projectId) {
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setIdProject(projectId);
        taskRepository.persist(task.getId(), task);
        return task;
    }


    @Override
    public void removeTaskById(@NotNull final String id) {
        if (taskRepository.findOne( id) != null) {
            taskRepository.remove(id);
        }

    }


    @Override
    public void updateTask(
            @NotNull final String id, @NotNull final String name, @NotNull final String description,
            @NotNull final String dataStart, @NotNull final String dataEnd, @NotNull final String projectId)
            throws ParseException {
        final Task task = taskRepository.findOne(id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        final Date updatedStartDate = DateFormatUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            task.setStatus(StatusEnum.PROGRESS);
        }
        task.setStartDate(updatedStartDate);
        final Date updatedEndDate = DateFormatUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            task.setStatus(StatusEnum.READY);
        }
        task.setEndDate(updatedEndDate);
        task.setIdProject(projectId);
        taskRepository.merge(id, task);
    }


    @Override
    @NotNull
    public List<Task> getAllTaskByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> resultTaskList = taskRepository.getAllTaskByProjectId(projectId);
        return resultTaskList;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        taskRepository.deleteAllTaskByProjectId(userId, projectId);
    }

    @Override
    public void clearTaskList() {
        taskRepository.removeAll();
    }


    @Override
    @NotNull
    public List<Task> searchByString(@NotNull final String userId, @NotNull final String string) {
        if (string.isEmpty()) return taskRepository.findAllByUserId(userId);
        @NotNull final List<Task> taskListByProjectId = taskRepository.searchByString(userId, string);
        return taskListByProjectId;
    }

    @Override
    public void removeAllTaskByUserId(@NotNull final String userId) {
        taskRepository.removeAllByUserId(userId);
    }
}
