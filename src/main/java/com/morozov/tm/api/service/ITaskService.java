package com.morozov.tm.api.service;

import com.morozov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    @Nullable Task findById(String id);

    @NotNull List<Task> findAllTask();

    @NotNull List<Task> getAllTask();

    @NotNull Task addTask(@NotNull String taskName, @NotNull String projectId);

    void removeTaskById(@NotNull String id);

    void updateTask(@NotNull String id, @NotNull String name,
                    @NotNull String description, @NotNull String dataStart, @NotNull String dataEnd,
                    @NotNull String projectId) throws ParseException;

    @NotNull List<Task> getAllTaskByProjectId(@NotNull String projectId);

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    void clearTaskList();

    @NotNull List<Task> searchByString(@NotNull String userId, @NotNull String string);

    void removeAllTaskByUserId(@NotNull String userId);
}
