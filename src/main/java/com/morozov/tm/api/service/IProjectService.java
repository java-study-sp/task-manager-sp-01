package com.morozov.tm.api.service;

import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    @Nullable Project findById(String id);

    @NotNull List<Project> findAllProject();

    @NotNull List<Project> getAllProjectByUserId(@NotNull String userId);

    @NotNull Project addProject(@NotNull String projectName);

    void removeProjectById(@NotNull String id);

    void updateProject(
            @NotNull String id, @NotNull String projectName,
            @NotNull String projectDescription, @NotNull String dataStart, @NotNull String dataEnd)
            throws ParseException;

    @NotNull List<Project> searchByString(@NotNull String userId, @NotNull String string);

    void removeAllByUserId(@NotNull String userId);

    void clearProjectList();
}
