package com.morozov.tm.api.repository;

import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IProjectRepository {

    @NotNull List<Project> findAll();

    @Nullable Project findOne(@NotNull String id);

    void merge(@NotNull String id, @NotNull Project updateEntity);

    void persist(@NotNull String id, @NotNull Project writeEntity);

    void remove(@NotNull String id);

    void removeAll();

    @NotNull List<Project> findAllByUserId(@NotNull String userId);

    @Nullable Project findOneByUserId(@NotNull String userId, @NotNull String id);

    @NotNull List<Project> searchByString(@NotNull String userId, @NotNull String string);

    void removeAllByUserId(@NotNull String userId);
}
