package com.morozov.tm.api.repository;

import com.morozov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ITaskRepository {
    @NotNull List<Task> findAll();

    @Nullable Task findOne(@NotNull String id);

    void merge(@NotNull String id, @NotNull Task updateEntity);

    void persist(@NotNull String id, @NotNull Task writeEntity);

    void remove(@NotNull String id);

    void removeAll();

    @NotNull List<Task> findAllByProjectIdUserId(@NotNull String userId, @NotNull String projectId);

    @NotNull List<Task> findAllByUserId(@NotNull String userId);

    @Nullable Task findOneByUserId(@NotNull String userId, @NotNull String id);

    @NotNull List<Task> getAllTaskByProjectId(@NotNull String projectId);

    void deleteAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    List<Task> searchByString(@NotNull String userId, @NotNull String string);

    void removeAllByUserId(@NotNull String userId);
}
